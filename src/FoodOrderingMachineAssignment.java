import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachineAssignment {
    private JPanel root;
    private JLabel toplabel;
    private JButton gyudonButton;
    private JButton tendonButton;
    private JButton katsudonButton;
    private JButton oyakodonButton;
    private JButton unadonButton;
    private JButton kaisendonButton;
    private JTextPane orderedItemsList;
    private JButton checkOutButton;
    private JTextPane totalprice;

int total=0;
    void order(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+ food +"?",
                "Ordered Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation==0){
            JOptionPane.showMessageDialog(null,"Order for "+food+" received.");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food+" "+price+" yen" + "\n");
           total+=price;
            totalprice.setText("Total:  "+total+" yen");

        }
    }

    public FoodOrderingMachineAssignment() {
        gyudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("gyudon.jpg")
        ));
        tendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("tendon.jpg")
        ));
        katsudonButton.setIcon(new ImageIcon(
                this.getClass().getResource("katsudon.jpg")
        ));
        oyakodonButton.setIcon(new ImageIcon(
                this.getClass().getResource("oyakodon.jpg")
        ));
        unadonButton.setIcon(new ImageIcon(
                this.getClass().getResource("unadon.jpg")
        ));
        kaisendonButton.setIcon(new ImageIcon(
                this.getClass().getResource("kaisendon.jpg")
        ));

        gyudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Gyudon",500);

            }
        });
        tendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Tendon",600);
            }
        });
        katsudonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Katsudon",600);
            }
        });
        oyakodonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Oyakodon",500);
            }
        });
        unadonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Unadon",1300);
            }
        });
        kaisendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Kaisendon",1500);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Ordered Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,"Thank you.The total is "+total+" yen.");

                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachineAssignment");
        frame.setContentPane(new FoodOrderingMachineAssignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
